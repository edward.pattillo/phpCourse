<?php

   // Medoo documentation: http://medoo.in/doc
  // include the medoo wrapper class
  require 'classes/medoo.php';
  require 'functions.php';

  $database = dbConnect();

/* -------------------------------------------------- */

  // get incoming POST values
  $queryType = $_POST['queryType'];
  $p1 = $_POST['p1'];
  $p2 = $_POST['p2'];
  $p3 = $_POST['p3'];

if ($queryType === 'search') {
    
    $searchString = $p1;
    $dateType = $p2;
    $searchDate = $p3;
  
    $songs = getSearchTracks($database, $searchString, $dateType, $searchDate);
  }  

  // if any of our queries returned an error we need to respond with that 
  // so that the AJAX jquery can catch the error
  checkForDatabaseError($database,$songs);

  // convert our multi-dimensional array to a JSON object 
  // and send it back to the browser
  $jsonSongs =  json_encode($songs);

// print_r($jsonSongs);
  echo $jsonSongs;



