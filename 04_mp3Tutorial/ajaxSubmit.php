<?php

 
  // insert info into database
   // Medoo documentation: http://medoo.in/doc
  // include the medoo wrapper class
  require 'classes/medoo.php';
  require 'functions.php';

  $database = dbConnect();

  


$failFlag = 0;

if (isset($_POST)) {

  $postArray = [];
  foreach($_POST as $key => $value) {
    
    $postArray[$key] = test_input($value);
  }
 
//   var_dump($postArray);
}



if (isset($postArray['action'])) {
  
  $trackID = $postArray['trackEditID'];
  
  if ($postArray['action'] == 'delete') {
    
    deleteTrack($database, $trackID);
    
  } else if ($postArray['action'] == 'update') {

    
    $artistInput = $postArray['artistInput'];
    $albumInput = $postArray['albumInput'];
    $trackInput = $postArray['trackInput'];
    
    updateInfo($database, $trackID, $artistInput, $albumInput, $trackInput);
    
   $trackIDs[]['trackID'] = $trackID;
   $songs = getAllTrackInfoFromTrackID($database, $trackIDs); 
    checkForDatabaseError($database,$songs); 
    $jsonSongs =  json_encode($songs);

  // print_r($jsonSongs);
    echo $jsonSongs;    
    
  }
  
  exit;
}




















if (isset($_FILES)) {
  
  $imgName = $_FILES['image']['name'];
  $fileType = $_FILES['image']['type'];
  $fileSize = $_FILES['image']['size'];
  
  if (strpos($fileType, 'image') === false) {
    echo 'You did not upload an image file.';
    $failFlag = 1;
  }
  
  
  if ($fileSize > 500000) {
    echo 'Your image file size was waaay to big.';
    $failFlag = 1;
  } 
  
  if ($failFlag !== 1) {
    $path_parts = pathinfo($imgName);
    $ext = $path_parts['extension'];

    $sourcePath = $_FILES['image']['tmp_name'];
    $targetPath = 'albumArt/'.$postArray['artistInput'].' - '.$postArray['albumInput'].'.'.$ext;
    echo getcwd();
    move_uploaded_file($sourcePath,$targetPath);
  }
}




function test_input($data) {
  
  $data = trim($data); // trim white space
  $data = stripslashes($data); // strip backwards slashes
  $data = htmlspecialchars($data); // convert special characters
  return $data;  
}




if ($failFlag !== 1) {

  
  $time = strtotime($postArray['yearInput']);
  $validDate = date('Y-m-d', $time);
   
  // ------------------------------------   MATCH or INSERT ARTIST ------------------------------------------
  
  $artistInfo = $database->select('artists', ['artistID'], ['artistName' => $postArray['artistInput']]);
  
//   print_r($artistInfo);
  
  if (empty($artistInfo)) {
    
    // insert artist name into the database
    $artistID = $database->insert('artists', ['artistName' => $postArray['artistInput']]);
    
  } else {
    
    $artistID = $artistInfo[0]['artistID'];
  }
  
//       echo $artistID;

  
    // ------------------------------------   MATCH or INSERT ALBUM ------------------------------------------

  $albumArtistCheck = FALSE;
  // start by checking if any album by the same name already exists
  $albumCheck = $database->select('albums', ['albumID'], ['albumName' => $postArray['albumInput']]);
  
  if(!empty($albumCheck)) {
    
    // loop through all of the albums (even though it might just be one)
    foreach ($albumCheck as $album) {
      
      $tempAlbumID = $album['albumID'];
      $tempArtistCheck = $database->select('artists_albums', ['artistID'], ['albumID' => $tempAlbumID]);
      
      if(!empty($tempArtistCheck)) {
        
        foreach($tempArtistCheck as $tArtist) {
          
          if ($tArtist['artistID'] == $artistID) {
            
            $albumArtistCheck = TRUE;
            $albumID = $tempAlbumID;
          }
        }
      }
    }
  }
  
  // if an album doesn't exist with the same name by the artist
  if ($albumArtistCheck === FALSE) {
    
    // insert album into database
    $albumID = $database->insert('albums', [
                                                'albumName' => $postArray['albumInput'],
                                                'albumReleaseDate' => $postArray['yearInput']
                                            ]);
    
    $albumArtistID = $database->insert('artists_albums', ['artistID' => $artistID, 'albumID' => $albumID ]);

  }
    // ------------------------------------   MATCH or INSERT TRACKS ------------------------------------------

  // start by checking if any tracks are already in the database on this particular album
  $trackCheck = $database->select('albums_tracks', ['trackID'], ['albumID' => $albumID]);
  
  if (!empty($trackCheck)) {
    
    foreach($trackCheck as $track) {
      
      $tempTrackID = $track['trackID'];
      $tempTrackCheck = $database->select('tracks', ['trackName'], ['trackID' => $tempTrackID]);
      
      if (!empty($tempTrackCheck)) {
        
        foreach($tempTrackCheck as $tempTrack) {
          
          $tempTrackName = $tempTrack['trackName'];
          
          foreach($postArray as $key => $incomingTrackName) {
            
            if(strstr($key, 'trackInput')) {
              
              if ($incomingTrackName == $tempTrackName) {
                
                // a track already exists
                echo 'One of your tracks already exists. Thank you for playing.';
                exit;
                
              } // END OF if ($incomingTrackName == $tempTrackName) {
            } // END OF if(strstr($key, 'trackInput')) {
          } // END OF foreach($postArray as $key => $incomingTrackName) {
        } // END OF  foreach($tempTrackCheck as $tempTrack) {
      } // END OF if (!empty($tempTrackCheck)) { 
    } // END OF foreach($trackCheck as $track) {
  } // END OF  if (!empty($trackCheck)) {
  
  
  foreach($postArray as $key => $incomingTrackName) {

    if(strstr($key, 'trackInput')) {
      
      // insert new track into database, will return a trackID
      $trackID = $database->insert('tracks', ['trackName' => $incomingTrackName]);
      
      $albumTrackID = $database->insert('albums_tracks', ['trackID' => $trackID, 'albumID' => $albumID]);
      
      $artistTrackID = $database->insert('artists_tracks', ['trackID' => $trackID, 'artistID' => $artistID]);

      $trackIDs[]['trackID'] = $trackID;
    }
  }
  
  
  // --------------------------  PRESENT RESULTS TO USER ----------------------------------

 $songs = getAllTrackInfoFromTrackID($database, $trackIDs);
  
  // if any of our queries returned an error we need to respond with that 
  // so that the AJAX jquery can catch the error
  checkForDatabaseError($database,$songs);

  // convert our multi-dimensional array to a JSON object 
  // and send it back to the browser
  $jsonSongs =  json_encode($songs);

// print_r($jsonSongs);
  echo $jsonSongs;

  
  
} // END OF if ($failFlag !== 1) {


