<!doctype html>
<html>

<head>
  <title>MP3 Collection</title>
  <link href='style.css' rel='stylesheet' type='text/css'>
  <!-- this gets the latest version of jQuery -->
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <!-- This loads the jQuery UI library and the needed CSS. -->
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

  <script src="js.js"></script>

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Raleway" rel="stylesheet">
</head>

<body>


  <div id="control" class="slide-in">
    <h2 class="orangeH2">MP3 Collection</h2>
    <div id="toggle"><i class="material-icons togBut">play_circle_outline</i></div>

    <div id="accordion">
      <h3>Search</h3>
      <div id="topAccordian">
        <input id="mainSearch" placeholder="Search by artist, album or song" value="Lenny Kravitz">
        <div class="iconButton" id="searchButton"><i class="material-icons">search</i></div>
        <div id="dateArea">
          Release date: 
          <select name="dateType" id="dateType">
            <option selected="selected">Any</option>
            <option>Before</option>
            <option>After</option>
          </select>
          <input type="text" id="searchDate">
        </div>
      </div>
      <h3>Enter Tracks</h3>
      <div id="bottomAccordian" class="ui-helper-clearfix">
        <input type="button" class="formButton" id="save" value="save" />
        <input type="button" class="formButton" id="update" value="update" />
        <input type="button" class="formButton" id="delete" value="delete" />
        <input type="hidden" id="trackEditID" value="">
        <input id="artistInput" placeholder="Artist">
        <input id="albumInput" placeholder="Album">
        <input type="date" id="yearInput" placeholder="Year">
        
        
        
        <div class="imageInputAreas">
          <input type="file" id="uploadFile" accept="image/*"/>
          <a id="fileUpButton">
          <br><br><br>
          <i class="material-icons">add_a_photo</i><br> Add image<br>(max 500px x 500px)
            </a>
        </div>
        
        
        
        <div class="trackInputAreas">
          <input class="trackInput" placeholder="Track name" id="trackInput-1">
          <i class="material-icons plusTrackButton" id="plusTrackButton-1">add_circle</i>
        </div>


      </div>
    </div>
  </div>

  <div id="trackArea"></div>

  <!--
  <div id="menu">
    <button type="button" id="clearTracks">Clear</button>
    <button type="button" class="filterButton" id="1">Metallica</button>
    <button type="button" class="filterButton" id="2">Edie Brickell & New Bohemians</button>
    <button type="button" class="filterButton" id="3">Lenny Kravitz</button>
    <button type="button" class="filterButton" id="4">Marvin Gaye</button>
    <button type="button" class="filterButton" id="5">Pink Floyd</button>
    <button type="button" class="filterButton" id="6">Bob Dylan</button>
    <button type="button" class="filterButton" id="7">Bob Marley & The Wailers</button>
    <button type="button" class="filterButton" id="getAllTrackInfoForDate" name="1987-01-01">Since 1987</button>
    <button type="button" class="filterButton" id="getAllTracks">All</button>

  </div>
  -->


</body>

</html>