// our event listeners go inside the document ready function
$(document).ready(function() {

  $('#mainSearch').focus();
  $('#delete').hide();
  $('#update').hide();
  
  var formData = new FormData();
  var isImageUploaded = 0;
  
  // ---------------------------------     jQuery UI widgets -------------------------------

  $("#accordion").accordion({
    animate: 200,
    autoFill: true,
    autoHeight: true
  });

  $('#searchDate').datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });

  var slider = document.getElementById('control');
  var trackArea = document.getElementById('trackArea');

  $("#toggle").click(function() {

    var isOpen = slider.classList.contains('slide-in');
    slider.setAttribute('class', isOpen ? 'slide-out' : 'slide-in');
    trackArea.setAttribute('class', isOpen ? 'trackArea-grow' : 'trackArea-shrink');
  });


  // ---------------------------------  event listeners -------------------------------

  
  $('#save').click(function() {
      
    var killFlag = 0;
 
    if(!isImageUploaded) {
      alert("You need to upload an image.");
      killFlag = 1;
    }
    
    if($('#artistInput').val()) {
      formData.append('artistInput', $('#artistInput').val());
    } else {
      alert("You need to fill in the artist's name.");
      killFlag = 1;
    }
     
    if($('#albumInput').val()) {
      formData.append('albumInput', $('#albumInput').val());
    } else {
      alert("You need to fill in the album's name.");
      killFlag = 1;
    }
     
    if($('#yearInput').val()) {
      formData.append('yearInput', $('#yearInput').val());
    } else {
      alert("You need to fill in the album's release date.");
      killFlag = 1;
    }
    
    var i = 0;
    $.each($('.trackInputAreas :input'), function() {
    
      if($(this).val()) {
        var trackID = this.id;
        var trackName = $(this).val();
        formData.append(trackID, trackName);
        i = 1;
      } else {
        if(i === 0) {
          alert("You need to fill in at least one track name.");
          killFlag = 1;
        }
      }
    });

    if (killFlag === 0) { submitAlbumInfo(formData); }
  });
 
  
  $('#fileUpButton').click(function() {
  
    $('#uploadFile').click();
  });
  
  
  $('#uploadFile').change(function() {
  
    var imgPath = $(this)[0].value;
//     console.log(imgPath);
    
    var extn = imgPath.substring(imgPath.lastIndexOf('.') +1).toLowerCase();
    
    if (extn == "gif" || extn == "jpg" || extn == "jpeg" || extn == "png" || extn == "bmp") {
      
//       console.log(extn);
      
      var reader = new FileReader();
      
      reader.onload = function(e) {
      
        // get loaded image and render a thumbnail
        $('.imageInputAreas').html('<img id="showImage" src="" width="210" height="210" />');
        $('#showImage').attr('src', e.target.result);
      };
      
      // read the image file as a data URL
      reader.readAsDataURL(this.files[0]);
      
      formData.append('image', this.files[0]);
      isImageUploaded = 1;
      
    } else {
      
      alert("Please select an image file.");
    }
    
  });
  
  
  $("body").on("click", ".plusTrackButton", function(e) {

//     console.log(this.id);
    var trackId = this.id;
    $('#' + trackId).fadeOut(100);
    var temp = trackId.split("-");
    var numericID = parseInt(temp[1]);
    var trackHTML = '<div class="trackInputAreas">';
    trackHTML += '<input class="trackInput" placeholder="Track" id="trackInput-' + (numericID + 1) + '">';
    trackHTML += '<i class="material-icons plusTrackButton"  id="plusTrackButton-' + (numericID + 1) + '">add_circle</i>';
    trackHTML += '</div>';
    $('#bottomAccordian').append(trackHTML);
  });


  $("#mainSearch").keypress(function(e) {

    var key = e.which;

    if (key == 13) {
      var searchString = $("#mainSearch").val();
      var dateType = $('#dateType option:selected').text();
      var searchDate = $("#searchDate").val();

      getTracks('search', searchString, dateType, searchDate);
    }
  });


  $("#searchButton").click(function() {

    var searchString = $("#mainSearch").val();
    var dateType = $('#dateType option:selected').text();
    var searchDate = $("#searchDate").val();

    getTracks('search', searchString, dateType, searchDate);
  });



  // event listener to clear the contents of the trackArea
  $('#clearTracks').click(function() {

    $('#trackArea').html('');

  }); // END OF $('#clearTracks').click(function() {

  
  $(document).on("click", ".track", function() {
    
    console.log(this.id);
    $('#ui-id-2').click();
    
    $('#delete').show();
    $('#update').show();    
    $('#save').hide();  
    
    var thisArtist = $('#'+this.id+' '+'.artistName').text();
    var thisTrack = $('#'+this.id+' '+'.trackName').text();
    var thisAlbum = $('#'+this.id+' '+'.trackInfo').attr('albumName');
    
    $('#artistInput').val(thisArtist);
    $('#trackInput-1').val(thisTrack);
    $('#albumInput').val(thisAlbum);
    $('#trackEditID').val(this.id);
    
  });

  
  $('#update').click(function() {
  
    var trackID = $('#trackEditID').val();
    
    formData.append('action', 'update');
    formData.append('artistInput', $('#artistInput').val());
    formData.append('albumInput', $('#albumInput').val());
    formData.append('trackInput', $('#trackInput-1').val());
    formData.append('trackEditID', trackID);
    
    $('#'+trackID).remove();
    submitAlbumInfo(formData);
    
  });
  
   
  $('#delete').click(function() {
  
    formData.append('action', 'delete');
    
    var trackID = $('#trackEditID').val();
    formData.append('trackEditID', trackID);
    
    $('#'+trackID).remove();
    submitAlbumInfo(formData);
    
  });
  
 
  
  
}); // END OF $(document).ready(function() {


// ---------------------- functions -------------------------------


// this function makes an AJAX request that gets tracks by a particular artist
function submitAlbumInfo(formData) {

  $.ajax({

      // specifices where the AJAX request will be sent to on the server
      url: "ajaxSubmit.php",

      // an object with the data we want to send to the receiving file on the server
      data: formData,
      processData: false,
      contentType: false,

      // type of request to send (either GET or POST)
      type: "POST",

      // type of data to expect back from the server
      // The available data types are text, html, xml, json, jsonp, and script.
      dataType: 'json'
//       dataType: 'text'

    })
    // .done defines what will happen when the ajax request is complete
    // we're going to name the response "returnData" and pass it to a function
    // that will present the track information
    .done(function(returnData) {

      console.log(returnData);
      if (returnData.length === 0) {

        presentNoResults();
      } else {
        $("#mainSearch").val('');
        //     console.log(returnData);
        $.each(returnData, function(key, trackInfo) {
          presentTrack(trackInfo);
        });
      }

    })
    // .fail defines what will happen when the ajax request fails and doesn't return a JSON object
    // we will have it present the error message returned from PHP at the top of the page
    .fail(function(returnData) {
      //     console.log(returnData);
      $('body').prepend(returnData.responseText);
    });

} //  END OF submitAlbumInfo


// this function makes an AJAX request that gets tracks by a particular artist
function getTracks(queryType, p1, p2, p3) {

  $.ajax({

      // specifices where the AJAX request will be sent to on the server
      url: "ajax.php",

      // an object with the data we want to send to the receiving file on the server
      data: {
        queryType: queryType,
        p1: p1,
        p2: p2,
        p3: p3
      },

      // type of request to send (either GET or POST)
      type: "POST",

      // type of data to expect back from the server
      // The available data types are text, html, xml, json, jsonp, and script.
      dataType: 'json'

    })
    // .done defines what will happen when the ajax request is complete
    // we're going to name the response "returnData" and pass it to a function
    // that will present the track information
    .done(function(returnData) {

      if (returnData.length === 0) {

        presentNoResults();
      } else {
        $("#mainSearch").val('');
        //     console.log(returnData);
        $.each(returnData, function(key, trackInfo) {
          presentTrack(trackInfo);
        });
      }

    })
    // .fail defines what will happen when the ajax request fails and doesn't return a JSON object
    // we will have it present the error message returned from PHP at the top of the page
    .fail(function(returnData) {
      //     console.log(returnData);
      $('body').prepend(returnData.responseText);
    });

} // END OF getTracks()


function presentNoResults() {

  // build a string that contains the HTML for presenting our track
  var trackHTML = '<div class="trackError">';
  trackHTML += '<div class="trackInfo">';
  trackHTML += 'No tracks found for ' + $("#mainSearch").val();
  trackHTML += '</div>';
  trackHTML += '</div>';

  // add this HTML as a child to the #trackArea div in our HTML
  $('#trackArea').append(trackHTML);
  $('.trackError').fadeOut(4000);
}

function presentTrack(trackInfo) {

  // build a string that contains the HTML for presenting our track
  var trackHTML = '<div class="track" id="'+trackInfo.trackID+'">';
  trackHTML += '<img src="albumArt/' + trackInfo.artistName + ' - ' + trackInfo.albumName + '.jpg" />';
  trackHTML += '<div class="trackInfo" albumName="'+trackInfo.albumName +'">';
  trackHTML += '<div class="artistName">'+trackInfo.artistName + '</div>';
  trackHTML += '<div class="trackName">'+trackInfo.trackName + '</div>';
  trackHTML += '</div>';
  trackHTML += '</div>';

  // add this HTML as a child to the #trackArea div in our HTML
  $('#trackArea').append(trackHTML);
}