-- phpMyAdmin SQL Dump
-- version 4.0.10.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 06, 2016 at 06:23 AM
-- Server version: 5.1.73
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mp3Collection`
--

-- --------------------------------------------------------

--
-- Table structure for table `tracks`
--

DROP TABLE IF EXISTS `tracks`;
CREATE TABLE IF NOT EXISTS `tracks` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT COMMENT 'primary key index',
  `trackName` varchar(255) NOT NULL,
  `trackArtist` varchar(255) NOT NULL,
  `trackAlbumName` varchar(255) NOT NULL,
  `releaseDate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tracks`
--

INSERT INTO `tracks` (`id`, `trackName`, `trackArtist`, `trackAlbumName`, `releaseDate`) VALUES
(1, 'Fight Fire With Fire', 'Metallica', 'Ride the Lightning', '1984-07-27'),
(2, 'Ride the Lightning', 'Metallica', 'Ride the Lightning', '1984-07-27'),
(3, 'For Whom the Bell Tolls', 'Metallica', 'Ride the Lightning', '1984-07-27');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
