# PHP Course

# ======================
 
some git commands for easy reference 

# make a new repo and link to one on gitlab

git clone git@gitlab.com:edward.pattillo/phpCourseExamples.git

(what comes after "git clone" comes from the project page on GitLab)

# make a branch called work1 

git checkout -b work1

# add all files

git add .

# commit files   

git commit -a -m 'comment'

# push to  work1

git push origin work1


# fast forward work 1 into master (must checkout master branch first)

git merge --ff-only work1   

# delete work 1

git branch -D work1 

# ======================
 

# some config setting

git config branch.autosetupmerge true


# gitlab videos

https://www.youtube.com/watch?v=7p0hrpNaJ14

https://www.youtube.com/watch?v=enMumwvLAug



# ======================



# Command line instructions


# Git global setup

git config --global user.name "Edward Pattillo"

git config --global user.email "edward.pattillo@nayland.school.nz"

# Create a new repository

git clone git@gitlab.com:edward.pattillo/REPONAME.git

cd chuck

touch README.md

git add README.md

git commit -m "add README"

git push -u origin master

# Existing folder

cd existing_folder

git init

git remote add origin git@gitlab.com:edward.pattillo/REPONAME.git

git add .

git commit

git push -u origin master


# Existing Git repository

cd existing_repo

git remote add origin git@gitlab.com:edward.pattillo/REPONAME.git

git push -u origin --all

git push -u origin --tags


# Overwrite master branch

git checkout current_branch

git merge -s ours master

git checkout master

git merge current_branch
