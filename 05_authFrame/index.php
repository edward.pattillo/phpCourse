<!doctype html>
<html>

<head>
  <!-- this allows us to use unicode characters -->
  <meta charset="UTF-8">
  <title>Authentication Framework</title>
  
  <!-- linking in our style sheet -->
  <link href='style.css' rel='stylesheet' type='text/css'>
  
  <!-- this gets the latest version of jQuery -->
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  
  <!-- linking in our javascript AFTER the jQuery library is loaded -->
  <script src="js.js"></script>
  
  <!-- loading some Google Fonts -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Raleway|Fascinate+Inline|Material+Icons" rel="stylesheet">
  
  <!-- This stores the address of the page so that we can use it to seed a randomly generated token. -->
  <!-- Notice the interchangeable use of javaScript and PHP. They work beautifully like that. -->
  <!-- And despite that I'm explaining this here, prying eyes have slim to no chance of guessing how I'll encrypt this -->
  <script> var URI = '<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>';</script>
</head>

<body>
  
  <!-- This sits at the top of the page. -->
  <div id="header">
  
    <div id="title">Authentication Framework</div>
 
    <!-- This is the navigation. Elements are shown and hidden depeding on the access permission of the user -->
    <div id="links"><span id="onlyAdmin"><a id="adminLink" href="#">admin</a> | 
      <a id="dashboardLink" href="#">dashboard</a> | </span>
      <a id="logOut" href="#">logout</a>
    </div>
  
  </div>
  
  <!-- Here's an empty message div. The server will send messages to it as required. -->
  <div id="message"></div>
  
  <!-- This is the login and registration section. It is shown and hidden as required via javaScript -->
  <div id="loginRego">
    <input id="email" placeholder="email"><br>
    <input id="password" type="password" placeholder="password"><br><br>
    <button id="loginRegoSubmit">Login</button> or
    <a id="regoLoginLink" href="#">Register</a><br>
    <div id="loginRegoMessage"></div>
  </div>

  
  <!-- This is the dashboard section. It is shown and hidden as required via javaScript -->
  <div id="dashboard">
    <h2 id="pageTitle">Dashboard</h2>
  </div>
    
  
  <!-- This is the admin section. It is shown and hidden as required via javaScript -->
  <div id="admin">
    <h2 id="pageTitle">Admin</h2>
    <div id="userDetailArea"></div>
  </div>
  
  
  
</body>

</html>