<?php
  // start a session
  // if the user hasn't logged in then no session variables will be set
  // so we can still restrict access by testing on session variables
  session_start();

/* ------------------------   connect to the database -------------------  */

// this sets the default time zone to NZ so that date values are stored in local time 
date_default_timezone_set("Pacific/Auckland");

// include the medoo wrapper
 include 'classes/medoo.php';

// create our $database object and connect to the database
  $database = new medoo([
  
    // required
    'database_type' => 'mysql',
    'database_name' => 'authFramework',
    'server' => 'localhost',
    'username' => 'DigitalDojo',
    'password' => 'ojoDlatigiD108',
    'charset' => 'utf8'
  ]);
 
  
/* ------------------------   validate and sanitise -------------------  */


// this flag is for all the validation
// if anything fails then it will be set to false
$validateFlag = true;

// assign all incoming AJAX post values to a variable
$incomingPostValues = $_POST;



// Here we declare an array to store all of the response information.
// The first value we set is the type of request being made
// so that when this is sent back to the client we can identify it
// and do something specific with the information according to the type of request made
// (ex: if the user is trying to log in, we can do that for them and then tell the client
// that they're authenticated and show them the user interface)
$response['request'] = $incomingPostValues['request'];




// if the user isn't trying to register or log in
// we won't waste any processing power on this request
// until we check if the session hasn't timed out
// and that the request contains an access token
// by sending some details off to the checkToken function below
// if it fails then no further code will be executed
// if it passes then we add the token to the response so that it stays alive

$maxSessionLength = 60 * 60; // session stays alive for one hour 
$inactivitySeconds = 60 * 60; // one hour is allowed between requests before user times out

if ($response['request'] !== 'rego' && $response['request'] !== 'login' && $response['request'] !== 'sessionDetails') {
  checkToken($incomingPostValues, $inactivitySeconds, $database);
}




// send each of the user input values  
// off to be validated and sanitised
foreach ($incomingPostValues as $field => $value) {
  
  // cleanFlag will be false if the field/value 
  // we are sending doesn't pass the rules for that field
  $cleanFlag = serverSideValidation($field, $value);
  
  // if any of the values fail validation then we set the overall validate flag to false
  if ($cleanFlag === FALSE) { $validateFlag = FALSE; }
}

// if the validate flag isn't false then we send the input data off
// to various functions to be dealt with individually depending on the request
if ($validateFlag !== false) {

  // if user is trying to register, send user input off to the submitRego function 
  if ($incomingPostValues['request'] == 'rego') {
    submitRego($response, $incomingPostValues, $database);
  }
  
  // if user is trying to login, send user input off to the submitLogin function 
  if ($incomingPostValues['request'] == 'login') {
    submitLogin($response, $incomingPostValues, $maxSessionLength, $database);
  }
  
  // if the client is asking for the session details
  if ($incomingPostValues['request'] == 'sessionDetails') {
    getSessionDetails($response);
  }
  
  // if the client is logging out
  if ($incomingPostValues['request'] == 'logOut') {
    logOut($response, $database);
  }
   
  // if the client wants the admin info containing user details and permissions
  if ($incomingPostValues['request'] == 'getAdminInfo') {
    getAdminInfo($response, $database);
  }
   
  // if the client wants to change admin permisssions for a user
  if ($incomingPostValues['request'] == 'changeAdminAccess') {
    changeAdminAccess($response, $incomingPostValues, $database);
  }
   
  // if the client wants to give or remove access permission for a user
  if ($incomingPostValues['request'] == 'changeAccess') {
    changeAccess($response, $incomingPostValues, $database);
  }
    
  // if the client wants to delete a user
  if ($incomingPostValues['request'] == 'deleteUser') {
    deleteUser($response, $incomingPostValues, $database);
  }
 
}



// ----------------------------- FUNCTIONS ---------------------------- 



// this function converts our $response array into JSON and sends it off to the client
function returnResponseToClient($response) {
  // the .done section of our AJAX function on the client side receives this echoed JSON object
  echo json_encode($response);
}








// this function submits a registration request into the database
// or throws an error if the account already exists
function submitRego($response, $incomingPostValues, $database) {
  
  // assign input email to a variable
  $email = $incomingPostValues['email'];
  
  // see if that user email already has a row in the user field
  $existingUser = $database->select('user', ['userEmail'], ['userEmail' => $email]);
  
  // if there are no values returned from the database it means the user doesn't exist yet
  if (count($existingUser) == 0) {
  
    // hash the user input pwd with the native default PHP hashing algorithm
    $hashedPwd = password_hash($incomingPostValues['password'], PASSWORD_DEFAULT);
    
    // make a new row in the user database with email and hashed password
    $database->insert('user', ['userEmail' => $email, 'userPassword' => $hashedPwd]);
    
    // send the user a message to confirm
    $response['message'] = 'Account for '.$email.' has been set up. The admin will, however, still need to give you permission.';
    returnResponseToClient($response);
    
  } else {
    // if the account already exists in the database throw an error
    $response['error'] = 'Error: Account for '.$email.' already exists.';
    returnResponseToClient($response);
  }
} // END OF function submitRego($incomingPostValues, $database)







// this function handles logins and starts a session if pwd and email match
function submitLogin($response, $incomingPostValues, $maxSessionLength, $database) {

  // assign input email to a variable
  $email = $incomingPostValues['email']; 
  $inputPwd = $incomingPostValues['password'];
  $URI = $incomingPostValues['URI'];

  // get all of the user details from the database
  $existingUser = $database->select('user', '*', ['userEmail' => $email]);

  // if the user is in the database
  if ($existingUser) {
    
    // set some variables. the [0] offset is because medoo returns an array of values
    // even though we should only have one matching email and thus one user array
    $hash = $existingUser[0]['userPassword'];
    $approved = $existingUser[0]['userAuth'];
    $userID = $existingUser[0]['userID'];

    // compare hashed pwd to input password
    if (password_verify($inputPwd, $hash)) {

      // just using "if ($variableName)" is a shortcut way to test for true or false for boolean data types only
      if ($approved) {

        // generate a hashed random access token using a string, the URI of the requesting page and a 50 digit RGN
        $randomSeed = "Jus! soMe h@rdc0ded sEEd ch@rac!Erz!".$URI.substr(md5(microtime()),rand(0,26),50);
        $token = hash('sha256', $randomSeed);
 
        // if the user is authorised to use the app, set some session variables
        // never put a hased password as a session variable as this will be exposed to the client
        $_SESSION["userEmail"] = $email;
        $_SESSION["userID"] = $userID;
        $_SESSION["userURI"] = $URI;
        $_SESSION["userAdmin"] = $existingUser[0]['userAdmin'];
        $_SESSION["userAuth"] = $existingUser[0]['userAuth'];
        $_SESSION["token"] = $token;
        
        // add session variables to the returned value
        $response['session'] = $_SESSION;
        
        // create a current datetime for mySql 
        $datetime = date_format(date_create(), 'Y-m-d H:i:s');
        
        // update the user row with timestamp to show time of last activity
        // and token for verification purposes
        $database->update('user', ['userLastRequest' => $datetime], ['userID' => $userID]);
        
        // send a message back to the client
        $response['message'] = $email.' is logged into system.';
        returnResponseToClient($response);

      } else {

        // if they aren't authorised: create an error message and send it off to the client
        $response['error'] = 'Error: Your login details are correct but you have not been given permission to use this application. Contact the admin.';
        returnResponseToClient($response);
      }
    } else {

      // create an error message and send it off to the client
      $response['error'] = 'Error: Invalid password.';
      returnResponseToClient($response);
    }
  } else {
    
    // create an error message and send it off to the client
    $response['error'] = 'Error: Account does not exist.';
    returnResponseToClient($response);
  }
      
} // END OF submitLogin($incomingPostValues, $database) {






// logOut is called
function logOut($response, $database) {
  
  // test if any session variables are set
  if (isset($_SESSION["userEmail"])) {
    
    $email = $_SESSION["userEmail"];

    // destroy session variables
    session_destroy();
     
    // send a goodbye emessage
    $response['message'] = $email.' is logged out of the system.';
    returnResponseToClient($response);    
    
  } else {

    // even if session variables aren't set we should destroy the session just for good feng shui
    session_destroy();
  }
}







// this function just checks whether or not a session 
// is active and gives the client the session details 
// or 'false' if the session isn't active
function getSessionDetails($response) {
     
  // one way to check if a user is logged in is to see if any session variables have been set
  if (isset($_SESSION['userEmail'])) {

    $response['session'] = $_SESSION;

  } else {

    $response['session'] = false;
  }
  
  // send response off to client
  returnResponseToClient($response);
}








// this is called when a user with admin permission changes access permission for another user
function changeAccess($response, $incomingPostValues, $database) {
  
  // we only give access to this section if the session is for an admin user
  if (isset($_SESSION['userAdmin']) && $_SESSION['userAdmin'] == true) {
  
    // set some variables based on incoming values
    $userID = $incomingPostValues['userID'];
    $accessState = $incomingPostValues['accessState'];
    
    // change the database row
    $database->update('user', ['userAuth' => $accessState], ['userID' => $userID]);
    
    // get the user's email address so we can make the outgoing message specific
    $user = $database->select('user', 'userEmail', ['userID' => $userID]);
    
    $response['message'] = 'Access permission changed for '.$user[0];
    
  } else {

    $response['error'] = 'You do not have permission to change this information.';
  }

  // send response off to client
  returnResponseToClient($response);
}







// this is called when a user with admin permission changes admin access permission for another user
function changeAdminAccess($response, $incomingPostValues, $database) {
  
  // we only give access to this section if the session is for an admin user
  if (isset($_SESSION['userAdmin']) && $_SESSION['userAdmin'] == true) {
  
    // set some variables based on incoming values
    $userID = $incomingPostValues['userID'];
    $adminState = $incomingPostValues['adminState'];
    
    // change the database row
    $database->update('user', ['userAdmin' => $adminState], ['userID' => $userID]);
    
    // get the user's email address so we can make the outgoing message specific
    $user = $database->select('user', 'userEmail', ['userID' => $userID]);
    
    $response['message'] = 'Admin permission changed for '.$user[0];
    
  } else {

    $response['error'] = 'You do not have permission to change this information.';
  }

  // send response off to client
  returnResponseToClient($response);
}








// this function checks if the user is an admin and queries 
// the database for user info then it sends it back to the client
function getAdminInfo($response, $database) {
  
  // we only give access to this section if the session is for an admin user
  if (isset($_SESSION['userAdmin']) && $_SESSION['userAdmin'] == true) {

    // see if that user email already has a row in the user field
    $response['userDetails'] = $database->select('user', ['userID', 'userEmail', 'userAdmin', 'userAuth']);
    
    $response['message'] = 'Here are the other users of the system.';
    
  } else {

    $response['error'] = 'You do not have permission to view this information.';
  }

  // send response off to client
  returnResponseToClient($response);
}







// this is called when a user with admin permission deletes another user
function deleteUser($response, $incomingPostValues, $database) {
  
  // we only give access to this section if the session is for an admin user
  if (isset($_SESSION['userAdmin']) && $_SESSION['userAdmin'] == true) {
  
    // set some variables based on incoming values
    $userID = $incomingPostValues['userID'];
    $userEmail = $incomingPostValues['userEmail'];
    
    // delete the database row
    $database->delete('user', ['userID' => $userID]);
    
    // add info to the response array to be send back to client
    $response['userID'] = $userID;
    $response['message'] = 'Deleted user '.$userEmail.' from the system.';
    
  } else {

    $response['error'] = 'You do not have permission to change this information.';
  }

  // send response off to client
  returnResponseToClient($response);
}







// this function tests the access token against the one stored in the database
// it also checks that the user has been active for at least a certain number of minutes
// if they fail these checks, they are sent back to the login screen
function checkToken($incomingPostValues, $inactivitySeconds, $database) {
  
  
  // set some variables based on incoming values
  $userEmail = $_SESSION['userEmail'];
  $userURI = $incomingPostValues['URI']; 
  $storedURI = $_SESSION["userURI"]; 


  // we only test the token if the session is active, the user has access permission
  // and the requesting URI matches the one stored in the session variable when established
  if (isset($_SESSION['userAuth']) && $_SESSION['userAuth'] == true && $userURI == $storedURI) {


    // test that the incoming token and stored token match
    if ($incomingPostValues['token'] == $_SESSION["token"]) {
      
      // if tokens match then we test that the last activity wasn't passed the allowed seconds
      
      // get the user's time of last request from database 
      $userDetails = $database->select('user', ['userLastRequest'], ['userEmail' => $userEmail]);

      // assign values retrieved from database
      $lastAccessed = $userDetails[0]['userLastRequest'];

      // convert datetimes to timestamps
      $date = date_create();
      $currentTimeStamp = date_format($date, 'U');
      $lastAccessedTimestamp = strtotime($lastAccessed);
      
      // add allowable inactive seconds to last access time
      $maxTime = $lastAccessedTimestamp + $inactivitySeconds;
      
      // check that the max allowed seconds haven't lapsed
      if ($currentTimeStamp <= $maxTime) {
         
        // if the time hasn't lapsed then update database so that we restart the timer
        $datetime = date_format($date, 'Y-m-d H:i:s');
        $database->update('user', ['userLastRequest' => $datetime], ['userEmail' => $userEmail]);
        
        // success!
        return;
        
      } else {
        // if the user has timed out
        $response['error'] = 'Your session has timed out.';
      }
    } else {
      // if tokens don't match
      $response['error'] = 'Your session needs to be restarted.';
    }
  } else {
    // no session variables are set
    $response['error'] = 'You are not logged in, or your session has expired.';
  }
     
  // kill this session if the request failed the tests
  session_destroy();
 
    // trick the client into logging out if token fails or user times out
  $response['request'] = 'logOut';
  
  // send response off to client if all the conditions fail
  returnResponseToClient($response);
  
  // terminate program so that no further requests are run
  // (we really don't like unauthorised attempts to inject our system)
  exit;
}








// this is a "one size fits all" validation
// and sanitization function. All inputs will need to 
// be run through this function to make sure no nastiness
// gets anywhere near our database
function serverSideValidation($field, $value) {
  
  // php strips all HTML tags
  $value = strip_tags($value);
  
  // this flag is for the specific field/value that we're testing
  $cleanFlag = true;
  
  
  
  // here is where we put the rules for the email value
  if ($field == 'email') {
    
    // test to make sure the email contains the nayland email string
    $testMail = "@nayland.school.nz";
    if (strpos($value, $testMail) === false) {
      
      // set the flag to fail so that we can catch it and respond when its returned
      $cleanFlag = false;
      $response['error'] = 'Error: email not valid.';
      returnResponseToClient($response);
    }
  }
  
  
  
  
  // here is where we put the rules for the password value
  if ($field == 'password') {
    
    $pwdMinLength = 6; // pwd has to have at least 6 chars
    
    // strlen counts the # of characters in the string
    $pwdLength = strlen($value);
    
    if ($pwdLength < $pwdMinLength) {
      
      // set the flag to fail so that we can catch it and respond when its returned
      $cleanFlag = false;
      $response['error'] = 'Error: password is not long enough. You need at least '.$pwdMinLength.' characters.';
      returnResponseToClient($response);
    }
  }
  
  
  

  // this will return true if the value passes
  // all the validation rules for its field
  return $cleanFlag;  
  
} // END OF function serverSideValidation($field, $value)




// ------------------------------------------------------------- 
 
    
    