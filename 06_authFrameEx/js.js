// our event listeners go inside the document ready function
// so that the code inside is available once the page has loaded
$(document).ready(function() { 


  // hide user interface elements until we know 
  // if the user is authenticated or an admin
  $('#links').hide();
  $('#dashboard').hide();
  $('#admin').hide();
  $('#loginRego').hide();

  // store details of the session here
  var sessionDetails = {};
  var token;

  // -------------- just for testing purposes --------------
  $('#email').val('edward.pattillo@nayland.school.nz');
  $('#password').val('aaaaaa');
  // -------------- just for testing purposes --------------

  // create an object to store info we want to send off to the server
  var formData = new FormData();

 
  // when the page loads, request the server to see if a session is active
  // set the request property to login so that we know what's being asked to happen when the data goes to the server
  formData.append('request', 'sessionDetails');
  // send formData to the ajax submit function 
  submitInfo(formData);
  
  

  
  
  

  // ----------------------------------- event listeners -----------------------------



  // this swaps the interface from a login state 
  // to a registration state and vice versa
  $('#regoLoginLink').click(function() {

    // we are checking if the interface is in 'login' mode or 'rego' mode
    var regoStatus = $('#loginRegoSubmit').text();

    if (regoStatus == 'Login') {

      // if the interface is in login mode, swap it to rego mode
      $(' <input id="passwordConfirm" type="password" placeholder="confirm password">').insertAfter("#password");
      $('#loginRegoSubmit').text('Register');
      $('#regoLoginLink').text('Login');

      // -------------- just for testing purposes --------------
      $('#passwordConfirm').val('aaaaaa');
      // -------------- just for testing purposes --------------


    } else {

      // if the interface is in rego mode, swap it to login mode
      $('#passwordConfirm').remove();
      $('#loginRegoSubmit').text('Login');
      $('#regoLoginLink').text('Register');
    }

  }); // END OF $('#regoLoginLink').click(function(){






  // this is triggered when someone clicks the submit/rego button
  $('#loginRegoSubmit').click(function() {


    // reset error message
    $('#loginRegoMessage').text('');


    // check button text to see what state the login/rego interface is in
    var regoStatus = $('#loginRegoSubmit').text();

    // if the interface is showing a login instead of rego
    if (regoStatus == 'Login') {

      // set the request property to login so that we know what's being asked to happen when the data goes to the server
      formData.append('request', 'login');
    
    } else {

      // set the request property to rego so that we know what's being asked to happen when the data goes to the server
      formData.append('request', 'rego');

    }

    // this is a flag to check if anything failed client-side validation
    var regoFailFlag = false;


    // send user inputted email off to be validated
    var emailSubmitted = $('#email').val();
    var emailClean = clientSideValidation('email', emailSubmitted);

    if (emailClean !== false) {

      // if email passes validation then add the value to formData object
      //console.log('Valid email');
      formData.append('email', emailSubmitted);

    } else {
      regoFailFlag = true;
    } // set overall validation fail flag to true


    // send user inputted email off to be validated
    var pwdSubmitted = $('#password').val();
    var pwdClean = clientSideValidation('password', pwdSubmitted);

    if (pwdClean !== false) {

      // if email passes validation then add the value to formData object
      //console.log('Valid pwd');
      formData.append('password', pwdSubmitted);

    } else {
      regoFailFlag = true;
    } // set overall validation fail flag to true


    // we only submit when all validation passes 
    if (regoFailFlag === false) {

      // send formData to the ajax submit function 
      submitInfo(formData);
    }

  }); // END OF $('#loginRegoSubmit').click(function(){





  // this is triggered when someone clicks the logOut link
  $('#logOut').click(function() {

    // set the request property to logOut so that we know what's being asked to happen when the data goes to the server
    formData.append('request', 'logOut');

    // send formData to the ajax submit function 
    submitInfo(formData);
  });



  // this is triggered when someone clicks the admin link
  $('#adminLink').click(function() {

    // set the request property to logOut so that we know what's being asked to happen when the data goes to the server
    formData.append('request', 'getAdminInfo');

    // send formData to the ajax submit function 
    submitInfo(formData);

    // show the admin panel
    $('#dashboard').hide();
    $('#admin').fadeIn();

  });




  // this is triggered when someone clicks the dashboard link
  $('#dashboardLink').click(function() {

    // set the request property to logOut so that we know what's being asked to happen when the data goes to the server
    formData.append('request', 'dashboard');

    // send formData to the ajax submit function 
    submitInfo(formData);

    // show the dashboard panel
    $('#admin').hide();
    $('#dashboard').fadeIn();
    
    
    // shows whatever is in the thing table  
    formData.append('request', 'showThings');
    // send formData to the ajax submit function 
    submitInfo(formData);
 
  });


  // this is triggered when someone clicks on one of the isAdmin checkboxes
  // we use a different style of listener becuase these elements were created programatically
  // we use mouseup because the checkbox will be in a different state when the mouse is released than when it is clicked
  // instead of # we use . because we are selecting everything with that class. one listener for several elements
  $(document).on("click", ".isAdmin", function() {

    // get the id of the individual element clicked (we need to do this because the listener is for a class
    var id = $('#' + this.id);

    // on each checkbox we've assigned it an attribute 'userID' which we need to send off to the database
    var userID = id.attr('userID');
    formData.append('userID', userID);
    //     console.log(userID); 

    // test the checked state of the checkbox
    // if it is checked, we're going to change the admin access to true or 1, else false or 0
    if (id.is(':checked')) {
      formData.append('adminState', 1);
    } else {
      formData.append('adminState', 0);
    }

    // set the request property to changeAdminAccess to tell the PHP what to do when the data goes to the server
    formData.append('request', 'changeAdminAccess');

    // send formData to the ajax submit function 
    submitInfo(formData);
  });




  // this is triggered when someone clicks on one of the hasAccess checkboxes
  // we use a different style of listener becuase these elements were created programatically
  // we use mouseup because the checkbox will be in a different state when the mouse is released than when it is clicked
  // instead of # we use . because we are selecting everything with that class. one listener for several elements
  $(document).on("click", ".hasAccess", function() {

    // get the id of the individual element clicked (we need to do this because the listener is for a class
    var id = $('#' + this.id);

    // on each checkbox we've assigned it an attribute 'userID' which we need to send off to the database
    var userID = id.attr('userID');
    formData.append('userID', userID);

    // test the checked state of the checkbox
    // if it is checked, we're going to change the access to true or 1, else false or 0
    if (id.is(':checked')) {
      formData.append('accessState', 1);
    } else {
      formData.append('accessState', 0);
    }

    // set the request property to changeAccess to tell the PHP what to do when the data goes to the server
    formData.append('request', 'changeAccess');

    // send formData to the ajax submit function 
    submitInfo(formData);
  });




  // this is triggered when someone clicks on one of the hasAccess checkboxes
  // we use a different style of listener becuase these elements were created programatically
  // we use mouseup because the checkbox will be in a different state when the mouse is released than when it is clicked
  // instead of # we use . because we are selecting everything with that class. one listener for several elements
  $(document).on("click", ".deleteUser", function() {

    // get the id of the individual element clicked (we need to do this because the listener is for a class
    var id = $('#' + this.id);

    // on each checkbox we've assigned it attributes 'userID' and 'userEmail' which we need to send off to the database
    var userID = id.attr('userID');
    formData.append('userID', userID);
    var userEmail = id.attr('userEmail');
    formData.append('userEmail', userEmail);

    // we need to make sure that it wasn't a misclick. if they click cancel this fizzles
    var confirmDelete = confirm("Are you sure that you want to delete the user "+userEmail+"?");

    if (confirmDelete === true) {
    // set the request property to changeAccess to tell the PHP what to do when the data goes to the server
    formData.append('request', 'deleteUser');

    // send formData to the ajax submit function 
    submitInfo(formData);
    }
  });




  
  
  
  

  // this is triggered when someone clicks the dashboard link
  $('#addThing').click(function() {

    
   
    // set the request property to addThing so that we know what's being asked to happen when the data goes to the server
    formData.append('request', 'addThing');
    
    var thingName = $("#thingName").val();
    var thingQuantity = $("#thingQuantity").val();
    var thingDesc = $("#thingDesc").val();
    
    formData.append('thingName', thingName);
    formData.append('thingQuantity', thingQuantity);
    formData.append('thingDesc', thingDesc);
        

    // send formData to the ajax submit function 
    submitInfo(formData);

  });


  
  
  //
  
  $(document).on("click", ".deleteThing", function() {

    // get the id of the individual element clicked (we need to do this because the listener is for a class)
    var id = $('#' + this.id);

    // on each checkbox we've assigned it attributes  
    var thingId = id.attr('thingId');
      
    console.log('What was clicked? '+thingId);

    // set the request property to deleteItem so that we know what's being asked to happen when the data goes to the server
    formData.append('request', 'deleteThing');
    formData.append('thingId', thingId);
      
    // send formData to the ajax submit function 
    submitInfo(formData);
    
  });
  
  
  
  
  

  // ----------------------------------- END OF event listeners -----------------------------

  // ------------------------------------- functions ----------------------------------

  // this function makes an AJAX request that gets tracks by a particular artist
  function submitInfo(formData) {

    // any time we send a request to the server, we clear the message text
    // unless it is a request like the ones listed in the conditional below
    
//     if (formData.request !== 'showThings') {
//       $('#message').text('');
//     }
    
    $('#message').css({
      "background-color": "#FFF4AA",
      "color": "#AA3939"
    });


    // we send the URI variable we created in the HTML head section off 
    // to be tested against the URI that originally established the session
    formData.append('URI', URI); 
    formData.append('token', token); 

    
    // ye olde AJAX function
    $.ajax({

      // specifies the URL where the AJAX will be sent to the server
      url: 'ajaxSubmit.php',

      // formData is an object that contains the data we want to send to the server
      data: formData,
      processData: false,
      contentType: false,

      // this specifies the type of request we're sending. Options: GET or POST
      type: 'POST',

      // this specifies the format of the data that will be returned
      //       dataType: 'text'
      dataType: 'json'


      // this is what executes when the AJAX returns from the server
    }).done(function(returnData) {

      // console.log(returnData);

      // clear all of the properties of the formData object 
      // so that all future requests will be fresh
      for (var prop in formData) delete formData[prop];

      // send the response JSON off to the handleAjaxResponse() function to be sorted
      handleAjaxResponse(returnData);

      // this is what executes when the AJAX fails
    }).fail(function(returnData) {

      // clear all of the properties of the formData object 
      // so that all future requests will be fresh
      for (var prop in formData) delete formData[prop];

      // this puts the error message to the top of the page
      $('#message').text(returnData.responseText);
    });
  } // END OF function submitInfo()



/* ------------------------------------------------------------------------------ */


  // this receives the JSON response from the server if the AJAX succeeds
  function handleAjaxResponse(returnData) {

    
    
    // check response for errors or messages
    if (returnData.hasOwnProperty('error')) {

      // return message to the beginning of the body tag (above everything else)
      $('#message').text(returnData.error).fadeIn();
      $('#message').css({
        "background-color": "#AA3939",
        "color": "#FFF4AA"
      });

    } else if (returnData.hasOwnProperty('message')) {

      // return message to the beginning of the body tag (above everything else)
      $('#message').text(returnData.message).fadeIn();
      
     console.log(returnData.message);
    }


    
    
    

    // if we've asked for session details
    if (returnData.request == 'sessionDetails' || returnData.request == 'login') {

      token = returnData.session.token;
      //console.log(token);
      
      // if the session variables are set then show the user the dashboard
      // hide the login div and show the links at the top
      if (returnData.hasOwnProperty('session') && returnData.session !== false) {

        $('#links').fadeIn();
        $('#dashboard').fadeIn();
        $('#loginRego').hide();
        $('#admin').hide();
        $('#onlyAdmin').hide();
        
        // assign the user's email address to the sessionDetails object
        sessionDetails.userEmail = returnData.session.userEmail;

        // show the admin link if the user has admin priviledges
        // to test for 0 we need to convert the text "0" to a numeric data type
        // so we use parseInt() 
        if (parseInt(returnData.session.userAdmin) !== 0) {

          $('#onlyAdmin').fadeIn();
        }
        

        // shows whatever is in the thing table when user loads the dashboard
        formData.append('request', 'showThings');
        // send formData to the ajax submit function 
        submitInfo(formData);


      } else {

        // if the session variables aren't set then show just the login/rego div
        $('#loginRego').fadeIn();

      }
    }

    
    
    
    
    
    
    
    

    if (returnData.request == 'showThings') {
 
      console.log(returnData);
      

        // start forming the output HTML that will be presented in the admin sections
        var outputHTML = '<table id="thingTable"><tr><th>#</th><th>name</th><th>description</th><th></th></tr>';

        // loop through each thing to add a row to our output table. 
        // "key" allows us to access each object (thing) within the whole object (thingsDetails)
        for (var key in returnData.thingsDetails) {
          
          // assign each thing as its own object "t"  for convenient referencing
          var t = returnData.thingsDetails[key];
 
          outputHTML += '<tr><td>'+t.thingQuantity+'</td>';
          outputHTML += '<td>'+t.thingName+'</td>';
          outputHTML += '<td>'+t.thingDescription+'</td>';
          
          outputHTML += '<td><a id="'+t.thingID+'-thingID" class="deleteThing"  thingId="'+t.thingID+'"  href="#" style="color:red">delete</a></td></tr>';
          
          
        }

        outputHTML += '</table>';
      
        // put the outputHTML into the browser
        $('#thingDisplay').html(outputHTML);
    }


    
    
    
    
    
    
    
    

    if (returnData.request == 'addThing') {
 
    
      $("#thingName").val('');
      $("#thingName").focus();
      $("#thingQuantity").val('');
      $("#thingDesc").val('');
      
      

        // shows whatever is in the thing table  
        formData.append('request', 'showThings');
        // send formData to the ajax submit function 
        submitInfo(formData);

      
    }


    if (returnData.request == 'deleteThing') {
 

        // shows whatever is in the thing table  
        formData.append('request', 'showThings');
        // send formData to the ajax submit function 
        submitInfo(formData);

      
    }

    
    
    
    
    
    
    
    

    if (returnData.request == 'logOut') {

      // clear all of the properties of the sessionDetails object 
      for (var prop in sessionDetails) delete sessionDetails[prop];
      
      // send user back to the login screen
      $('#links').fadeOut();
      $('#dashboard').hide();
      $('#loginRego').fadeIn();
      $('#admin').hide();
    }


    
    

    // if the user just deleted another user
    if (returnData.request == 'deleteUser') {

      // fade out that row in the admin table
      var userID = returnData.userID;
      $('#'+userID).fadeOut();
    }

 



    // if the admin has requested the page
    if (returnData.request == 'getAdminInfo') {

      // if the server has responded with userDetails
      if (returnData.hasOwnProperty('userDetails') && returnData.userDetails !== false) {

        // start forming the output HTML that will be presented in the admin sections
        var outputHTML = '<table id="userTable"><tr><th>user</th><th>admin</th><th>access</th><th></th></tr>';

        // loop through each user to add a row to our output table. 
        // "key" allows us to access each object (user) within the whole object (userDetails)
        for (var key in returnData.userDetails) {

          // assign each user as its own object "u"  for convenient referencing
          var u = returnData.userDetails[key];

          // if the u isn't the user currently logged in we create a new row to present to the user
          // we don't present the currently logged in user's row because they can remove their own
          // access permission. Not showing them their details is a quick way to avoid a problematic situation.
          if (u.userEmail != sessionDetails.userEmail) {
            
            // the first column in our table is the user's email address
            outputHTML += '<tr id="'+u.userID+'"><td>' + u.userEmail + '</td>';

            // the second column in our table contains a checkbox that is only checked if the user has admin priviledges
            outputHTML += '<td><input id="' + u.userID + '_IsAdmin" class="isAdmin" userID="' + u.userID + '" type="checkbox"';
                        
            // the 1 would come directly from our database if the userAdmin field is set to true (1)
            if (u.userAdmin == 1) { outputHTML += 'checked'; }
            outputHTML += '><label for="' + u.userID + '_IsAdmin"></label></td>';

            // the third column in our table contains a checkbox that is only checked if the user has access to the system
            outputHTML += '<td><input id="' + u.userID + '_HasAccess" class="hasAccess" userID="' + u.userID + '" type="checkbox" ';
            
            // the 1 would come directly from our database if the userAuth field is set to true (1)
            if (u.userAuth == 1) {  outputHTML += 'checked'; }
            outputHTML += '><label for="' + u.userID + '_HasAccess"></label></td>';

            // the fourth column in our table contains a delete icon
            outputHTML += '<td><a id="' + u.userID + '_delete" class="deleteUser" userEmail="' + u.userEmail + '" userID="' + u.userID + '" href="#"><i class="material-icons">delete_forever</i></a></td></tr>';
          }
        } // end of for (var key in users) {

        // this will go onto the end of our HTML string regardless of how many rows we've dynamically created
        outputHTML += '</table>';

        // put the outputHTML into the browser
        $('#userDetailArea').html(outputHTML);
      }
    }

    
  } // END OF function handleAjaxResponse(returnData) {










  // this function checks that the user inputs are valid
  // it is not a security feature because it is client-side
  // it's really just here for the user's convenience
  // parameters are: field (the field we want to check) and value (the user input value)
  function clientSideValidation(field, value) {

    // console.log(field+' : '+value);
    // the value is considered "innocent until proven guilty"
    var cleanFlag = true;

    
    
    // check that the email value is valid
    if (field == 'email') {

      // this checks whether or not the email contains a school address
      var validString = value.search("@nayland.school.nz");

      //console.log('validString : '+validString);

      if (validString == -1) {
        // if the email address fails validation we need to tell the user and set the flag to false
        cleanFlag = false;
        $('#loginRegoMessage').text('Email account not valid.');
      }
    }
    

    
    // check that the password value is valid
    if (field == 'password') {

      // if they are registering test that the #passwordConfirm field matches the password entered
      // we're testing if the passwordConfirm field exists or not. it only exists if the user is trying to register
      if (document.getElementById('passwordConfirm')) {

        var pwdConfirm = $('#passwordConfirm').val();

        if (value != pwdConfirm) {
          cleanFlag = false;
          $('#loginRegoMessage').text('Passwords do not match.');
        }
      }


      // test that the password has enough characters
      var valueLength = value.length;

      if (valueLength < 6) {
        cleanFlag = false;
        $('#loginRegoMessage').text('Password needs to be at least 6 characters long.');
      }

    }



    // this will return either true or false
    return cleanFlag;
  } // END OF function clientSideValidation() {



  // ----------------------------------- END OF functions -----------------------------




}); // END OF $(document).ready(function() {