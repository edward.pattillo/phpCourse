<!doctype html>
<html>

<head>
  <title>Open Tags</title>
</head>

<body>
  
  <?php   
  
    echo '<h2>Opening and closing PHP tags on different lines.</h2>';   

      echo '<h2>String spans onto
      
      
      different lines.</h2>';   

    $interruptText = 'interrupted';
  
  ?>


  <?php   echo '<h2>Opening and closing PHP tags all in one line</h2>';   ?>

  <p>
    Here's some regular HTML.
  </p>

  <p>
    Here's some regular HTML <?=$interruptText?> by some PHP.
  </p>

  <!-- This is an HTML comment. -->
  
  <?php
  
  // this is a single line comment with two forward slashes
  
  # this is a single line comment with a single hash tag
  
  
  
  /*  this is a 
  
  multiline
  
  comment   */
 
  
  
  ?>
  
</body>

</html>